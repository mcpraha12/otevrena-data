'----------------------------------------------------------------------------------------------------
' Toto makro jiz neni pouzivane a nadale se neudrzuje. Misto nej se vyvoj presunul do skriptu v 
' JavaScriptu na https://mcpraha12.gitlab.io/otevrena-data/
'----------------------------------------------------------------------------------------------------

' definice vyctu moznych datovych typu sloupcu
Public Enum OD_DataTyp
    odtRetezec
    odtDatum
    odtCislo
End Enum

' definice typu pro udaje o zdrjovem sloupci
Public Type OD_ExportCol
    excSloupec As String      'souradnice v tabulce Excelu, napr "B" - neni nutno vyplnovat, pokud se vyplni NazevPuvodni
    NazevPuvodni As String    'nazev sloupce v puvodni tabulce (podle nej se da hledat) - neni nutno vyplnovat, pokud se vyplni excSloupec
    NazevSloupce As String    'nazev sloupce pro otevrena data
    DataTyp As OD_DataTyp     ' vysledny datovy typ v otevrenych datech
End Type

Sub OD_FormatBunek(DataTyp As OD_DataTyp, ByVal Sloupec As Integer, PocetRadek As Integer)
'
' Pomocna funkce na formatovani bunek ze vstupnich dat do stylu vyzadovanem otevrenou normalni formou
'
    On Error GoTo errorHandler
    
    Dim i As Integer
    Dim val As String
    
    For i = 2 To PocetRadek
        If IsEmpty(Cells(i, Sloupec)) Then GoTo PokracovatCyklem
        
        If DataTyp = odtDatum Then
            ' zmena formatu data na yyyy-mm-dd dle otevrene normalni formy
            ' nekdy je datum napsano spatne, pokusime se o konverzi, pri chybe to skoci do errorHandleru
            val = Format(CDate(Cells(i, Sloupec)), "yyyy-mm-dd")
        ElseIf DataTyp = odtCislo And IsNumeric(Cells(i, Sloupec)) Then
            ' export cisel s desetinnou teckou podle normy, funkce Str nepoziva desetinou carku
            val = Str(Cells(i, Sloupec))
        Else
            ' ostatnich pripadech prevzit obsah bunky
            val = Cells(i, Sloupec).Value
        End If
        
        ' nastavit format vsech bunek na textovy, aby se z cisel zase nedelala cisla s desetinnou carkou
        If DataTyp <> odtRetezec Then
            Cells(i, Sloupec).NumberFormat = "@"
        End If
        ' nastaveni noveho obsahu bunky
        Cells(i, Sloupec).FormulaR1C1 = val
        
PokracovatCyklem:
    Next i

errorHandler:
    If Err = 13 Then        'Type Mismatch, v pripade konverze data ve funkci CDate
        Resume PokracovatCyklem
    End If
End Sub

Sub OD_HledaniSloupcu(SloupceProExport() As OD_ExportCol, iPosledniSloupec As Integer, iRadekSNazvy As Integer)
'
' Pomocna funkce pro hledani sloupcu ve vstupni tabulce podle nazvu na danem radku. Vyzaduje mit vyplneno OD_ExportCol.NazevPuvodni
'
    Dim i, j, iPocetExpSloupcu As Integer
    
    ' zjisteni poctu sloupcu pro export, tedy delky pole SloupceProExport
    iPocetExpSloupcu = UBound(SloupceProExport)
    
    Dim iNalezen As Integer
    Dim sNazev As String
    
    For i = 1 To iPocetExpSloupcu
        sNazev = SloupceProExport(i).NazevPuvodni
        iNalezen = -1                               ' index sloupce z pole definic pro export, -1 znamena, ze zatim nebyl nalezen
        For j = 1 To iPosledniSloupec
            If sNazev = Cells(iRadekSNazvy, j).FormulaR1C1 Then
                iNalezen = j
            End If
        Next j
        
        If iNalezen = -1 Then
            MsgBox "Kriticka chyba: sloupec s naznem """ & sNazev & """ nebyl nalezen. Zkontrolujte NazevPuvodni v poli sloupcu pro export."
        Else
            SloupceProExport(i).excSloupec = Split(Cells(, iNalezen).Address, "$")(1)
        End If
    Next i
End Sub

Sub OD_TransformaceSloupcu(SloupceProExport() As OD_ExportCol, iPosledniSloupec As Integer, iPosledniRadek As Integer)
'
' Pomocna funkce na formatovani sloupcu, co maji byt exportovany, odmazani ostatnich sloupcu ve vstupni tabulce
'
    Dim i, j, iPocetExpSloupcu As Integer
    
    ' zjisteni poctu sloupcu pro export, tedy delky pole SloupceProExport
    iPocetExpSloupcu = UBound(SloupceProExport)
    
    Dim iNalezen As Integer
    Dim sNazev As String
    
    ' odzadu projit tabulku a smazat, pokud neni v seznamu - kvuli mazani se jde odzadu
    For i = iPosledniSloupec To 1 Step -1
        sNazev = Split(Cells(, i).Address, "$")(1)  ' nazev prochazeneho sloupce - napr "D"
        iNalezen = -1                               ' index sloupce z pole definic pro export, -1 znamena, ze zatim nebyl nalezen
        For j = 1 To iPocetExpSloupcu
            If sNazev = SloupceProExport(j).excSloupec Then
                iNalezen = j
            End If
        Next j
        
        ' pokud sloupec tabulky nebyl nalezen, smazat jej
        If iNalezen = -1 Then
            ' odmazat sloupec, co nema byt exportovan
            Columns(i).EntireColumn.Delete
        Else
            ' prejmenovat nazev sloupce pro export pro otevrena data
            Cells(1, i).FormulaR1C1 = SloupceProExport(iNalezen).NazevSloupce
            ' naformatovat vsechny bunky v exportovanem sloupci
            OD_FormatBunek DataTyp:=SloupceProExport(iNalezen).DataTyp, Sloupec:=i, PocetRadek:=iPosledniRadek
        End If
    Next i
End Sub

Sub OD_VlozitMenu(SloupecMena As Integer, Jmeno As String, PocetRadek As Integer)
'
' Pomocna funkce na vlozeni sloupce s menou u castek
'
    Dim i As Integer
    
    Columns(SloupecMena).Select
    Selection.Insert Shift:=xlToLRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' nastavi na prvni radek noveho sloupce nazev sloupce
    Cells(1, SloupecMena).FormulaR1C1 = Jmeno
    
    ' do zbyvajicich radku novebo sloupce vyplnit text "CZK"
    For i = 2 To PocetRadek
        Cells(i, SloupecMena).FormulaR1C1 = "CZK"
    Next i
End Sub

Sub OD_UlozitDoCSV(NazevSouboru As String)
'
' Pomocna funkce na ukladani tabulky do CSV
'
    Application.DisplayAlerts = False ' nejdriv vypnout otazku na to, jestli prepsat existujici soubor
    
    Dim Slozka As String
    Slozka = Left(ActiveWorkbook.FullName, InStrRev(ActiveWorkbook.FullName, "\"))

    ActiveWorkbook.SaveAs Filename:=Slozka & NazevSouboru, FileFormat:=xlCSV, CreateBackup:=False, local:=False

    Application.DisplayAlerts = True
End Sub

'----------------------------------------------------------------------------------------------------
'----------------------------------------------------------------------------------------------------
' Pomocna funkce pro prevod ze vstupni tabulky do vystupu
Sub OD_OZP_harmonogamTransform(ZdrojList As Worksheet, ExpList As Worksheet, PocetRadek As Integer, iSloupecLok As Integer, bMaUcel As Boolean)
    
    Const colDatum = 1
    
    Dim i As Integer
    Dim iExpRadek As Integer
    Dim sAktLok, sAktUcel As String
    Dim sAktDatumOd, sAktDatumDo, sAktCasOd, sAktCasDo As String
    
    Dim sNovyLok, sNovyUcel As String
    Dim sNovyDatum, sNovyCasInt, sNovyCasOd, sNovyCasDo As String
    Dim arrNovyCasSplit() As String
    
    sAktLok = ""
    sAktCasOd = ""
    sAktCasDo = ""
    sAktUcel = ""
    sNovyDatum = ""
    sNovyCasDo = ""
    sNovyUcel = ""
    
    iExpRadek = ExpList.Cells(Rows.Count, "a").End(xlUp).Row + 1
    
    For i = 2 To PocetRadek
        sNovyLok = ZdrojList.Cells(i, iSloupecLok).FormulaR1C1
        
        If sNovyLok <> "" Then
            ' nabrat si nove hodnoty
            sNovyDatum = Format(CDate(ZdrojList.Cells(i, 1)), "yyyy-mm-dd")
            sNovyCasInt = ZdrojList.Cells(i, iSloupecLok - 1).FormulaR1C1 ' casovy interval je sloupec pred naznem lokace
            If bMaUcel Then
                sNovyUcel = ZdrojList.Cells(i, iSloupecLok + 1).FormulaR1C1
            End If
            
            ' rozdelit casovy interval na dve casi
            arrNovyCasSplit = Split(sNovyCasInt, "-")
            If UBound(arrNovyCasSplit) = 1 Then
                sNovyCasOd = Trim(arrNovyCasSplit(0)) & ":00"
                sNovyCasDo = Trim(arrNovyCasSplit(1)) & ":00"
            Else
                MsgBox ("Chyba v deleni casoveho intervalu pres pomlcku: " & sNovyCasInt)
                Exit Sub
            End If
        End If
        
        If sNovyLok <> sAktLok Then
            ' jina hodnota - dat do vystupu hotovou lokaci z prubeznych promennych
            If sAktLok <> "" Then
                ' nastavit datumove sloupce jako text, aby se uchhoval ve formatu pro otevrena data
                ExpList.Cells(iExpRadek, 2).NumberFormat = "@"
                ExpList.Cells(iExpRadek, 3).NumberFormat = "@"
                ExpList.Cells(iExpRadek, 4).NumberFormat = "@"
                ExpList.Cells(iExpRadek, 5).NumberFormat = "@"
                
                ExpList.Cells(iExpRadek, 1).FormulaR1C1 = sAktLok
                ExpList.Cells(iExpRadek, 2).FormulaR1C1 = sAktDatumOd
                ExpList.Cells(iExpRadek, 3).FormulaR1C1 = sAktCasOd
                ExpList.Cells(iExpRadek, 4).FormulaR1C1 = sAktDatumDo
                ExpList.Cells(iExpRadek, 5).FormulaR1C1 = sAktCasDo
                If bMaUcel Then
                    ExpList.Cells(iExpRadek, 6).FormulaR1C1 = sAktUcel
                End If
                
                iExpRadek = iExpRadek + 1
            End If
            
            ' nabrat si nove hodnoty do prubeznych promennych
            sAktLok = sNovyLok
            If sNovyLok <> "" Then
                sAktDatumOd = sNovyDatum
                sAktCasOd = sNovyCasOd
                sAktDatumDo = sNovyDatum
                sAktCasDo = sNovyCasDo
                sAktUcel = sNovyUcel
            End If
            
        ElseIf sNovyLok = sAktLok And sNovyLok <> "" Then
            ' stejna hodnota - dat dohromady datumy z predchoziho radu
            sAktDatumDo = sNovyDatum
            sAktCasDo = sNovyCasDo
        End If
        
    Next i
    
End Sub

'----------------------------------------------------------------------------------------------------
'----------------------------------------------------------------------------------------------------
Sub OpenData_OZP_harmonogram()
'
' Makro na prevod tabulky s harmonogramy pristavovani kontejneru do formatu pro otevrena data
'
    '-----------------------------
    ' definice pro lokace
    Const iPocetSloupcuGPS = 3                          ' pocet sloucu, ktere se berou ze zdrojove tabulky
    Dim arrSloupceGPS(iPocetSloupcuGPS) As OD_ExportCol ' seznam definic vsech sloupcu ze zdrojove tabulky
    
    arrSloupceGPS(1).excSloupec = "A"
    arrSloupceGPS(1).NazevSloupce = "název"
    arrSloupceGPS(1).DataTyp = odtRetezec
    
    arrSloupceGPS(2).excSloupec = "B"
    arrSloupceGPS(2).NazevSloupce = "pozice_long"
    arrSloupceGPS(2).DataTyp = odtCislo
    
    arrSloupceGPS(3).excSloupec = "C"
    arrSloupceGPS(3).NazevSloupce = "pozice_lat"
    arrSloupceGPS(3).DataTyp = odtCislo

    '-----------------------------
    ' a ted prace
    ' zjistit rozmery vstupni tabulky
    Dim iPosledniSloupec As Integer
    Dim iPosledniRadek As Integer
    
    Worksheets("Bioodpad").Activate
    iPosledniRadek = Cells(Rows.Count, "a").End(xlUp).Row
    
    ' vytvorit novy list se zpracovanymi daty
    Sheets.Add.Name = "BioTx"
    Cells(1, 1).FormulaR1C1 = "stanoviště"
    Cells(1, 2).FormulaR1C1 = "datum_od"
    Cells(1, 3).FormulaR1C1 = "čas_od"
    Cells(1, 4).FormulaR1C1 = "datum_do"
    Cells(1, 5).FormulaR1C1 = "čas_do"
    Cells(1, 6).FormulaR1C1 = "materiál"
    
    ' postupne do noveho listu zkopirovat jednotlive sloupce s daty
    OD_OZP_harmonogamTransform ZdrojList:=Worksheets("Bioodpad"), ExpList:=Worksheets("BioTx"), PocetRadek:=iPosledniRadek, iSloupecLok:=4, bMaUcel:=True
    OD_OZP_harmonogamTransform ZdrojList:=Worksheets("Bioodpad"), ExpList:=Worksheets("BioTx"), PocetRadek:=iPosledniRadek, iSloupecLok:=7, bMaUcel:=True
    OD_OZP_harmonogamTransform ZdrojList:=Worksheets("Bioodpad"), ExpList:=Worksheets("BioTx"), PocetRadek:=iPosledniRadek, iSloupecLok:=10, bMaUcel:=True
    OD_OZP_harmonogamTransform ZdrojList:=Worksheets("Bioodpad"), ExpList:=Worksheets("BioTx"), PocetRadek:=iPosledniRadek, iSloupecLok:=13, bMaUcel:=True
    ' seradit cely list vzestupne podle data pristaveni
    Range("A1:F" & Cells(Rows.Count, "a").End(xlUp).Row).Sort Key1:=Range("B1"), Order1:=xlAscending, Header:=xlYes
    
    ' a nakonec ulozeni tabulky do CSV oddelenem carkami
    OD_UlozitDoCSV NazevSouboru:="opendata-harmonogram-odpady-bioodpad-MC-Praha-12.csv"
    
    '-----------------------------
    ' nyni podobne pro VOK
    Worksheets("VOK").Activate
    iPosledniRadek = Cells(Rows.Count, "a").End(xlUp).Row
    
    Sheets.Add.Name = "VokTx"
    Cells(1, 1).FormulaR1C1 = "stanoviště"
    Cells(1, 2).FormulaR1C1 = "datum_od"
    Cells(1, 3).FormulaR1C1 = "čas_od"
    Cells(1, 4).FormulaR1C1 = "datum_do"
    Cells(1, 5).FormulaR1C1 = "čas_do"
    
    OD_OZP_harmonogamTransform ZdrojList:=Worksheets("VOK"), ExpList:=Worksheets("VokTx"), PocetRadek:=iPosledniRadek, iSloupecLok:=4, bMaUcel:=False
    OD_OZP_harmonogamTransform ZdrojList:=Worksheets("VOK"), ExpList:=Worksheets("VokTx"), PocetRadek:=iPosledniRadek, iSloupecLok:=7, bMaUcel:=False
    Range("A1:E" & Cells(Rows.Count, "a").End(xlUp).Row).Sort Key1:=Range("B1"), Order1:=xlAscending, Header:=xlYes
    
    OD_UlozitDoCSV NazevSouboru:="opendata-harmonogram-odpady-vok-MC-Praha-12.csv"
    
    '-----------------------------
    ' nyni s klasickou transformaci tabulka pro lokace a GPS
    Worksheets("lokace-gps").Activate
    Cells.ClearFormats
    iPosledniSloupec = Cells(1, Columns.Count).End(xlToLeft).Column
    iPosledniRadek = Cells(Rows.Count, "a").End(xlUp).Row
    OD_TransformaceSloupcu SloupceProExport:=arrSloupceGPS, iPosledniSloupec:=iPosledniSloupec, iPosledniRadek:=iPosledniRadek
    OD_UlozitDoCSV NazevSouboru:="opendata-harmonogram-odpady-lokace-MC-Praha-12.csv"
    
    ' nyni je treba vsechny vysledne soubory otevrit v Poznamkovem bloku a zmenit kodovani znaku z ANSI na UTF-8
End Sub
