' definice vyctu moznych datovych typu sloupcu
Public Enum OD_DataTyp
    odtRetezec
    odtDatum
    odtCislo
End Enum

' definice typu pro udaje o zdrjovem sloupci
Public Type OD_ExportCol
    excSloupec As String      'souradnice v tabulce Excelu, napr "B" - neni nutno vyplnovat, pokud se vyplni NazevPuvodni
    NazevPuvodni As String    'nazev sloupce v puvodni tabulce (podle nej se da hledat) - neni nutno vyplnovat, pokud se vyplni excSloupec
    NazevSloupce As String    'nazev sloupce pro otevrena data
    DataTyp As OD_DataTyp     ' vysledny datovy typ v otevrenych datech
End Type

Sub OD_FormatBunek(DataTyp As OD_DataTyp, ByVal Sloupec As Integer, PocetRadek As Integer)
'
' Pomocna funkce na formatovani bunek ze vstupnich dat do stylu vyzadovanem otevrenou normalni formou
'
    On Error GoTo errorHandler
    
    Dim i As Integer
    Dim val As String
    
    For i = 2 To PocetRadek
        If IsEmpty(Cells(i, Sloupec)) Then GoTo PokracovatCyklem
        
        If DataTyp = odtDatum Then
            ' zmena formatu data na yyyy-mm-dd dle otevrene normalni formy
            ' nekdy je datum napsano spatne, pokusime se o konverzi, pri chybe to skoci do errorHandleru
            val = Format(CDate(Cells(i, Sloupec)), "yyyy-mm-dd")
        ElseIf DataTyp = odtCislo And IsNumeric(Cells(i, Sloupec)) Then
            ' export cisel s desetinnou teckou podle normy, funkce Str nepoziva desetinou carku
            val = Str(Cells(i, Sloupec))
        Else
            ' ostatnich pripadech prevzit obsah bunky
            val = Cells(i, Sloupec).Value
        End If
        
        ' nastavit format vsech bunek na textovy, aby se z cisel zase nedelala cisla s desetinnou carkou
        If DataTyp <> odtRetezec Then
            Cells(i, Sloupec).NumberFormat = "@"
        End If
        ' nastaveni noveho obsahu bunky
        Cells(i, Sloupec).FormulaR1C1 = val
        
PokracovatCyklem:
    Next i

errorHandler:
    If Err = 13 Then        'Type Mismatch, v pripade konverze data ve funkci CDate
        Resume PokracovatCyklem
    End If
End Sub

Sub OD_HledaniSloupcu(SloupceProExport() As OD_ExportCol, iPosledniSloupec As Integer, iRadekSNazvy As Integer)
'
' Pomocna funkce pro hledani sloupcu ve vstupni tabulce podle nazvu na danem radku. Vyzaduje mit vyplneno OD_ExportCol.NazevPuvodni
'
    Dim i, j, iPocetExpSloupcu As Integer
    
    ' zjisteni poctu sloupcu pro export, tedy delky pole SloupceProExport
    iPocetExpSloupcu = UBound(SloupceProExport)
    
    Dim iNalezen As Integer
    Dim sNazev As String
    
    For i = 1 To iPocetExpSloupcu
        sNazev = SloupceProExport(i).NazevPuvodni
        iNalezen = -1                               ' index sloupce z pole definic pro export, -1 znamena, ze zatim nebyl nalezen
        For j = 1 To iPosledniSloupec
            If sNazev = Cells(iRadekSNazvy, j).FormulaR1C1 Then
                iNalezen = j
            End If
        Next j
        
        If iNalezen = -1 Then
            MsgBox "Kriticka chyba: sloupec s naznem """ & sNazev & """ nebyl nalezen. Zkontrolujte NazevPuvodni v poli sloupcu pro export."
        Else
            SloupceProExport(i).excSloupec = Split(Cells(, iNalezen).Address, "$")(1)
        End If
    Next i
End Sub

Sub OD_TransformaceSloupcu(SloupceProExport() As OD_ExportCol, iPosledniSloupec As Integer, iPosledniRadek As Integer)
'
' Pomocna funkce na formatovani sloupcu, co maji byt exportovany, odmazani ostatnich sloupcu ve vstupni tabulce
'
    Dim i, j, iPocetExpSloupcu As Integer
    
    ' zjisteni poctu sloupcu pro export, tedy delky pole SloupceProExport
    iPocetExpSloupcu = UBound(SloupceProExport)
    
    Dim iNalezen As Integer
    Dim sNazev As String
    
    ' odzadu projit tabulku a smazat, pokud neni v seznamu - kvuli mazani se jde odzadu
    For i = iPosledniSloupec To 1 Step -1
        sNazev = Split(Cells(, i).Address, "$")(1)  ' nazev prochazeneho sloupce - napr "D"
        iNalezen = -1                               ' index sloupce z pole definic pro export, -1 znamena, ze zatim nebyl nalezen
        For j = 1 To iPocetExpSloupcu
            If sNazev = SloupceProExport(j).excSloupec Then
                iNalezen = j
            End If
        Next j
        
        ' pokud sloupec tabulky nebyl nalezen, smazat jej
        If iNalezen = -1 Then
            ' odmazat sloupec, co nema byt exportovan
            Columns(i).EntireColumn.Delete
        Else
            ' prejmenovat nazev sloupce pro export pro otevrena data
            Cells(1, i).FormulaR1C1 = SloupceProExport(iNalezen).NazevSloupce
            ' naformatovat vsechny bunky v exportovanem sloupci
            OD_FormatBunek DataTyp:=SloupceProExport(iNalezen).DataTyp, Sloupec:=i, PocetRadek:=iPosledniRadek
        End If
    Next i
End Sub

Sub OD_VlozitMenu(SloupecMena As Integer, Jmeno As String, PocetRadek As Integer)
'
' Pomocna funkce na vlozeni sloupce s menou u castek
'
    Dim i As Integer
    
    Columns(SloupecMena).Select
    Selection.Insert Shift:=xlToLRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' nastavi na prvni radek noveho sloupce nazev sloupce
    Cells(1, SloupecMena).FormulaR1C1 = Jmeno
    
    ' do zbyvajicich radku novebo sloupce vyplnit text "CZK"
    For i = 2 To PocetRadek
        Cells(i, SloupecMena).FormulaR1C1 = "CZK"
    Next i
End Sub

Sub OD_UlozitDoCSV(NazevSouboru As String)
'
' Pomocna funkce na ukladani tabulky do CSV
'
    Application.DisplayAlerts = False ' nejdriv vypnout otazku na to, jestli prepsat existujici soubor
    
    Dim Slozka As String
    Slozka = Left(ActiveWorkbook.FullName, InStrRev(ActiveWorkbook.FullName, "\"))

    ActiveWorkbook.SaveAs Filename:=Slozka & NazevSouboru, FileFormat:=xlCSV, CreateBackup:=False, local:=False

    Application.DisplayAlerts = True
End Sub

'----------------------------------------------------------------------------------------------------
'----------------------------------------------------------------------------------------------------
Sub OpenData_faktury()
'
' Makro na prevod tabulky z KDF z OEK do formatu pro otevrena data
'
    Const iPocetSloupcu = 14        ' pocet sloucu, ktere se berou ze zdrojove tabulky
    Dim arrSloupce(iPocetSloupcu) As OD_ExportCol ' seznam definic vsech sloupcu ze zdrojove tabulky
    
    arrSloupce(1).NazevPuvodni = "Kat."
    arrSloupce(1).NazevSloupce = "typ_transakce"
    arrSloupce(1).DataTyp = odtRetezec
    
    arrSloupce(2).NazevPuvodni = "Ag. číslo"
    arrSloupce(2).NazevSloupce = "agendové_číslo"
    arrSloupce(2).DataTyp = odtRetezec
    
    arrSloupce(3).NazevPuvodni = "IČO"
    arrSloupce(3).NazevSloupce = "dodavatel_ičo"
    arrSloupce(3).DataTyp = odtRetezec
    
    arrSloupce(4).NazevPuvodni = "DIČ"
    arrSloupce(4).NazevSloupce = "dodavatel_dič"
    arrSloupce(4).DataTyp = odtRetezec
    
    arrSloupce(5).NazevPuvodni = "Částka"
    arrSloupce(5).NazevSloupce = "částka_s_dph_výše"
    arrSloupce(5).DataTyp = odtCislo
    
    arrSloupce(6).NazevPuvodni = "Měna"
    arrSloupce(6).NazevSloupce = "částka_s_dph_výše_měna"
    arrSloupce(6).DataTyp = odtRetezec
    
    arrSloupce(7).NazevPuvodni = "K úhradě v CZK"
    arrSloupce(7).NazevSloupce = "částka_uhrazená_výše"
    arrSloupce(7).DataTyp = odtCislo
    
    arrSloupce(8).NazevPuvodni = "Dat. úhr."
    arrSloupce(8).NazevSloupce = "datum_úhrady"
    arrSloupce(8).DataTyp = odtDatum
    
    arrSloupce(9).NazevPuvodni = "Identifikátor"
    arrSloupce(9).NazevSloupce = "identifikátor_faktury"
    arrSloupce(9).DataTyp = odtRetezec
    
    arrSloupce(10).NazevPuvodni = "Kniha"
    arrSloupce(10).NazevSloupce = "kniha"
    arrSloupce(10).DataTyp = odtRetezec
    
    arrSloupce(11).NazevPuvodni = "Název subjektu"
    arrSloupce(11).NazevSloupce = "dodavatel"
    arrSloupce(11).DataTyp = odtRetezec
    
    arrSloupce(12).NazevPuvodni = "Popis"
    arrSloupce(12).NazevSloupce = "účel_platby"
    arrSloupce(12).DataTyp = odtRetezec
    
    arrSloupce(13).NazevPuvodni = "ID sml./obj."
    arrSloupce(13).NazevSloupce = "identifikátor_smlouvy"
    arrSloupce(13).DataTyp = odtRetezec
    
    arrSloupce(14).NazevPuvodni = "Ag. č. sml./obj."
    arrSloupce(14).NazevSloupce = "agendové_číslo_smlouvy"
    arrSloupce(14).DataTyp = odtRetezec
    
    ' odstranit prvni tri radky se zahlavim
    Rows(1).EntireRow.Delete
    Rows(1).EntireRow.Delete
    Rows(1).EntireRow.Delete
    
    ' smazat komplet formatovani v celem sesitu
    Cells.ClearFormats
    
    ' zjistit rozmery vstupni tabulky
    Dim iPosledniSloupec As Integer
    Dim iPosledniRadek As Integer
    
    iPosledniSloupec = Cells(1, Columns.Count).End(xlToLeft).Column
    iPosledniRadek = Cells(Rows.Count, "a").End(xlUp).Row
    
    ' najit spravne adresy sloupcu podle nazvu na radku 1
    OD_HledaniSloupcu SloupceProExport:=arrSloupce, iPosledniSloupec:=iPosledniSloupec, iRadekSNazvy:=1
    
    ' formatovani sloupcu, co maji byt exportovany, odmazani ostatnich sloupcu ve vstupni tabulce
    OD_TransformaceSloupcu SloupceProExport:=arrSloupce, iPosledniSloupec:=iPosledniSloupec, iPosledniRadek:=iPosledniRadek
    
    ' vlozeni CZK za castky
    'OD_VlozitMenu SloupecMena:=6, Jmeno:="částka_s_dph_měna", PocetRadek:=iPosledniRadek
    OD_VlozitMenu SloupecMena:=8, Jmeno:="částka_uhrazená_měna", PocetRadek:=iPosledniRadek
    
    ' a nakonec ulozeni tabulky do CSV oddelenem carkami
    OD_UlozitDoCSV NazevSouboru:="opendata-faktury-MC-Praha-12.csv"
    
    ' nyni je treba vysledny soubor otevrit v Poznamkovem bloku a zmenit kodovani znaku z ANSI a UTF-8
End Sub

'----------------------------------------------------------------------------------------------------
'----------------------------------------------------------------------------------------------------
Sub OpenData_smlouvy()
'
' Makro na prevod tabulky z SML z OEK do formatu pro otevrena data
'
    Const iPocetSloupcu = 13        ' pocet sloucu, ktere se berou ze zdrojove tabulky
    Dim arrSloupce(iPocetSloupcu) As OD_ExportCol ' seznam definic vsech sloupcu ze zdrojove tabulky
    
    arrSloupce(1).NazevPuvodni = "Identifikátor"
    arrSloupce(1).NazevSloupce = "identifikátor_smlouvy"
    arrSloupce(1).DataTyp = odtRetezec
    
    arrSloupce(2).NazevPuvodni = "Agendové číslo"
    arrSloupce(2).NazevSloupce = "agendové_číslo"
    arrSloupce(2).DataTyp = odtRetezec
    
    arrSloupce(3).NazevPuvodni = "Evidenční číslo"
    arrSloupce(3).NazevSloupce = "evidenční_číslo"
    arrSloupce(3).DataTyp = odtRetezec
    
    arrSloupce(4).NazevPuvodni = "Popis"
    arrSloupce(4).NazevSloupce = "popis"
    arrSloupce(4).DataTyp = odtRetezec
    
    arrSloupce(5).NazevPuvodni = "Měna"
    arrSloupce(5).NazevSloupce = "částka_měna"
    arrSloupce(5).DataTyp = odtRetezec
    
    arrSloupce(6).NazevPuvodni = "Celková částka"
    arrSloupce(6).NazevSloupce = "částka_výše"
    arrSloupce(6).DataTyp = odtCislo
    
    arrSloupce(7).NazevPuvodni = "Uzavřeno"
    arrSloupce(7).NazevSloupce = "datum_uzavření"
    arrSloupce(7).DataTyp = odtDatum
    
    arrSloupce(8).NazevPuvodni = "Ukončení platnosti"
    arrSloupce(8).NazevSloupce = "konec_platnosti"
    arrSloupce(8).DataTyp = odtDatum
    
    arrSloupce(9).NazevPuvodni = "Účinnost"
    arrSloupce(9).NazevSloupce = "datum_účinnosti"
    arrSloupce(9).DataTyp = odtDatum
    
    arrSloupce(10).NazevPuvodni = "Typ dokladu"
    arrSloupce(10).NazevSloupce = "typ_dokladu"
    arrSloupce(10).DataTyp = odtRetezec
    
    arrSloupce(11).NazevPuvodni = "Název"
    arrSloupce(11).NazevSloupce = "dodavatel"
    arrSloupce(11).DataTyp = odtRetezec
    
    arrSloupce(12).NazevPuvodni = "IČO"
    arrSloupce(12).NazevSloupce = "dodavatel_ičo"
    arrSloupce(12).DataTyp = odtRetezec
    
    arrSloupce(13).NazevPuvodni = "Organizační jednotka"
    arrSloupce(13).NazevSloupce = "organizacni-jednotka"
    arrSloupce(13).DataTyp = odtRetezec
    
    ' odstranit prvni dva radky se zahlavim
    Rows(1).EntireRow.Delete
    Rows(1).EntireRow.Delete
    
    ' smazat komplet formatovani v celem sesitu
    Cells.ClearFormats
    
    ' zjistit rozmery vstupni tabulky
    Dim iPosledniSloupec As Integer
    Dim iPosledniRadek As Integer
    
    iPosledniSloupec = Cells(1, Columns.Count).End(xlToLeft).Column
    iPosledniRadek = Cells(Rows.Count, "a").End(xlUp).Row
    
    ' najit spravne adresy sloupcu podle nazvu na radku 1
    OD_HledaniSloupcu SloupceProExport:=arrSloupce, iPosledniSloupec:=iPosledniSloupec, iRadekSNazvy:=1
    
    ' formatovani sloupcu, co maji byt exportovany, odmazani ostatnich sloupcu ve vstupni tabulce
    OD_TransformaceSloupcu SloupceProExport:=arrSloupce, iPosledniSloupec:=iPosledniSloupec, iPosledniRadek:=iPosledniRadek
    
    ' a nakoenc ulozeni tabulky do CSV oddelenem carkami
    OD_UlozitDoCSV NazevSouboru:="opendata-smlouvy-MC-Praha-12.csv"
    
    ' nyni je treba vysledny soubor otevrit v Poznamkovem bloku a zmenit kodovani znaku z ANSI a UTF-8
End Sub

'----------------------------------------------------------------------------------------------------
'----------------------------------------------------------------------------------------------------
Sub OpenData_objednavky()
'
' Makro na prevod tabulky z SML z OEK do formatu pro otevrena data. Je vlastne totozne jako OpenData_smlouvy krome zamen slova "smlouvy" na "objednavky"
'
    Const iPocetSloupcu = 13        ' pocet sloucu, ktere se berou ze zdrojove tabulky
    Dim arrSloupce(iPocetSloupcu) As OD_ExportCol ' seznam definic vsech sloupcu ze zdrojove tabulky
    
    arrSloupce(1).NazevPuvodni = "Identifikátor"
    arrSloupce(1).NazevSloupce = "identifikátor_objednávky"
    arrSloupce(1).DataTyp = odtRetezec
    
    arrSloupce(2).NazevPuvodni = "Agendové číslo"
    arrSloupce(2).NazevSloupce = "agendové_číslo"
    arrSloupce(2).DataTyp = odtRetezec
    
    arrSloupce(3).NazevPuvodni = "Evidenční číslo"
    arrSloupce(3).NazevSloupce = "evidenční_číslo"
    arrSloupce(3).DataTyp = odtRetezec
    
    arrSloupce(4).NazevPuvodni = "Popis"
    arrSloupce(4).NazevSloupce = "popis"
    arrSloupce(4).DataTyp = odtRetezec
    
    arrSloupce(5).NazevPuvodni = "Měna"
    arrSloupce(5).NazevSloupce = "částka_měna"
    arrSloupce(5).DataTyp = odtRetezec
    
    arrSloupce(6).NazevPuvodni = "Celková částka"
    arrSloupce(6).NazevSloupce = "částka_výše"
    arrSloupce(6).DataTyp = odtCislo
    
    arrSloupce(7).NazevPuvodni = "Uzavřeno"
    arrSloupce(7).NazevSloupce = "datum_uzavření"
    arrSloupce(7).DataTyp = odtDatum
    
    arrSloupce(8).NazevPuvodni = "Ukončení platnosti"
    arrSloupce(8).NazevSloupce = "konec_platnosti"
    arrSloupce(8).DataTyp = odtDatum
    
    arrSloupce(9).NazevPuvodni = "Účinnost"
    arrSloupce(9).NazevSloupce = "datum_účinnosti"
    arrSloupce(9).DataTyp = odtDatum
    
    arrSloupce(10).NazevPuvodni = "Typ dokladu"
    arrSloupce(10).NazevSloupce = "typ_dokladu"
    arrSloupce(10).DataTyp = odtRetezec
    
    arrSloupce(11).NazevPuvodni = "Název"
    arrSloupce(11).NazevSloupce = "dodavatel"
    arrSloupce(11).DataTyp = odtRetezec
    
    arrSloupce(12).NazevPuvodni = "IČO"
    arrSloupce(12).NazevSloupce = "dodavatel_ičo"
    arrSloupce(12).DataTyp = odtRetezec
    
    arrSloupce(13).NazevPuvodni = "Organizační jednotka"
    arrSloupce(13).NazevSloupce = "organizacni-jednotka"
    arrSloupce(13).DataTyp = odtRetezec
    
    ' odstranit prvni dva radky se zahlavim
    Rows(1).EntireRow.Delete
    Rows(1).EntireRow.Delete
    
    ' smazat komplet formatovani v celem sesitu
    Cells.ClearFormats
    
    ' zjistit rozmery vstupni tabulky
    Dim iPosledniSloupec As Integer
    Dim iPosledniRadek As Integer
    
    iPosledniSloupec = Cells(1, Columns.Count).End(xlToLeft).Column
    iPosledniRadek = Cells(Rows.Count, "a").End(xlUp).Row
    
    ' najit spravne adresy sloupcu podle nazvu na radku 1
    OD_HledaniSloupcu SloupceProExport:=arrSloupce, iPosledniSloupec:=iPosledniSloupec, iRadekSNazvy:=1
    
    ' formatovani sloupcu, co maji byt exportovany, odmazani ostatnich sloupcu ve vstupni tabulce
    OD_TransformaceSloupcu SloupceProExport:=arrSloupce, iPosledniSloupec:=iPosledniSloupec, iPosledniRadek:=iPosledniRadek
    
    ' a nakoenc ulozeni tabulky do CSV oddelenem carkami
    OD_UlozitDoCSV NazevSouboru:="opendata-objednavky-MC-Praha-12.csv"
    
    ' nyni je treba vysledny soubor otevrit v Poznamkovem bloku a zmenit kodovani znaku z ANSI a UTF-8
End Sub
