# Skripty a makra pro práci s otevřenými daty

Městská část Praha 12 publikuje otevřená data, konkrétně na pražském portálu [opendata.praha.eu](https://opendata.praha.eu/organization/praha-12).

Přizpůsobením celostátního vzoru publikovanou Ministerstvem vnitra na webu [opendata.gov.cz](https://opendata.gov.cz/standardy:start) byla vytvořena  směrnice na publikování a katalogizaci otevřených dat (text je přílohou usnesení [R-039-005-19](https://www.praha12.cz/assets/File.ashx?id_org=80112&id_dokumenty=71703)). V únoru 2020 pak byl schválen první publikační plán (viz usnesení [R-064-021-20](https://www.praha12.cz/assets/File.ashx?id_org=80112&id_dokumenty=75344)), v březnu 2021 pak byly přidány další sady (viz usnesení [R-112-017-21](https://www.praha12.cz/assets/File.ashx?id_org=80112&id_dokumenty=82166)).
  

### Jak dostat makra z našeho GitLabu do vašeho Excelu

Zde publikujeme makra čistě jako zdrojové kódy, pro jejich spuštění je třeba je mít na svém počítači v nějakém excelovém sešitu.

Doporučuji mít makra ve zvláštním sešitu a ten otevřít kdykoli je třeba provádět generování otevřených dat. Tedy v novém sešitu si nejdříve spustit editor maker a následně jej uložit (s příponou XLSM).

Viz oficiální návody:
- [Rychlý start: vytvoření makra](https://support.microsoft.com/cs-cz/office/rychlý-start-vytvořen%C3%AD-makra-741130ca-080d-49f5-9471-1e5fb3d581a8)
- [Uložení makra](https://support.microsoft.com/cs-cz/office/uložen%C3%AD-makra-24a026ef-3145-4bf8-a5f2-2fc7889ff74a)

Spouštění maker se pak může dělat tak, že si otevřete sešit k makry, pak sešit se zdrojovými daty a na něm spustíte příslušné makro (v Excelu lze spustit makro z jiného otevřeného sešitu).

## Ekonomická data

Pro generování ekonomických datových sad (faktury, objednávky, smlouvy) se používají makra napsaná v Excelu, tedy v jazyku Visual Basic for Applications. Praha 12 používá ekonomický systém Ginis, makra jsou pak spouštěna nad Excelovými sestavami exportovanými z Ginisu.

Aby CSV soubor byl ve formátu, který očekává česká norma pro otevřená data (tedy datumy jako RRRR-MM-DD, čísla s desetinnou tečkou a kódování UTF-8), pro usnadnění makro vedle odmazání nepotřebných sloupců z tabulky z Ginisu převede data do tohoto tvaru (kromě kódování, které verze Excelu používaná na Praze 12 neumí).

### Faktury - postup

1. Export z GINISu:
    - modul KDF
    - Režim knih: Všechny knihy v aktuálním roce -&gt; seznam
    - tisk Operativní tisk – standardní
    - Reportér – převést na XLSX
2. Otevřete výsledný soubor v Excelu (zde je možná třeba ještě ručně provést anonymizaci dat) a vedle toho i Excelový soubor s makry
3. Spusťte makro OpenData_faktury, udělá převod a uloží tabulku jako soubor ve formátu CSV.
4. Nakonec je třeba změnit kódování výsledného souboru. Otevřete jej v Poznámkovém bloku a změňte kódování z ANSI na UTF-8 (tedy menu Soubor - Uložit jako, a dole vybrat jiné kódování).

### Smlouvy a Objednávky - postup

1. Export z GINISu:
    - modul SML
    - Režim knih: Všechny knihy v aktuálním roce -&gt; seznam
    - tisk Výběr dokladů z knihy . graficky
    - Přehled vybraných dokladů – export do MS Excel
2. Otevřete výsledný soubor v Excelu a vedle toho i Excelový soubor s makry
3. Spusťte makro OpenData_smlouvy (příp. OpenData_objednavky), udělá převod a uloží tabulku jako soubor ve formátu CSV.
4. Nakonec je třeba změnit kódování výsledného souboru. Otevřete jej v Poznámkovém bloku a změňte kódování z ANSI na UTF-8 (tedy menu Soubor - Uložit jako, a dole vybrat jiné kódování).

## Harmonogramy přistavování velkoobjemových kontejnerů

Městská část Praha 12 pro své občany přistavuje velkoobjemové kontejnery na bioodpad. Jsou placené z rozpočtu městské části, proto nejsou v centrálním pražském systému KSNKO.

Tabulka s plánem na přistavování kontejnerů je vedena v Excelu (najdete ji v adresáři example) a je orientována hlavně pro přehlednost pro pracovníka, který se zabývá plánováním přistavování kontejnerů. Nad tabulkou je spouštěn skript, který uloží harmonogramy jako otevřená data ve formátu CSV. Dříve byl skript ve VBA, nyní se používá JavaScript.

### Postup

1. Otevřete webovou stránku [otevrena-data/kontejnery](https://mcpraha12.gitlab.io/otevrena-data/kontejnery.html) na záložce Data z XLSX.
2. Vyberte zdrojový Excelový sešit.
3. Skript vygeneruje tři potřebné CSV soubory, které si uložte a takto pojmenované je nahrajte na [opendata.praha.eu](https://opendata.praha.eu)

- - -

# Skripty na zpracování otevřených dat

V adresáři `public` se nachází jednoduché webové stránky na technologii GitLab Pages, které následně běží na [mcpraha12.gitlab.io/otevrena-data](https://mcpraha12.gitlab.io/otevrena-data/). Momentálně se na nich nachází skript na generování výstupu z datové sady pro harmonogramy přistavování velkoobjemových kontejnerů, který se dá použít pro webové stránky městské části nebo Noviny Prahy 12.
