//---------------------------------------------------------------------
// Initialise the underlaying map using Leaflet API. Call after page load, before data processing.
var g_aOdtMap;
function odtInitP12Map()
{
    // Init maps with Leaflet
    const MAPY_API_KEY = "ptZVPljDvizQ0cegplSTPFbdOj2NZzIkOxg_QfZT_Jo";
    var map = L.map('m').setView([49.996, 14.44], 13);
    const tileLayers = {
      'Základní': L.tileLayer(`https://api.mapy.cz/v1/maptiles/basic/256/{z}/{x}/{y}?apikey=${MAPY_API_KEY}`, {
        minZoom: 0,
        maxZoom: 19,
        attribution: '<a href="https://api.mapy.cz/copyright" target="_blank">&copy; Seznam.cz a.s. a další</a>',
    }),
      'Letecká': L.tileLayer(`https://api.mapy.cz/v1/maptiles/aerial/256/{z}/{x}/{y}?apikey=${MAPY_API_KEY}`, {
        minZoom: 0,
        maxZoom: 19,
        attribution: '<a href="https://api.mapy.cz/copyright" target="_blank">&copy; Seznam.cz a.s. a další</a>',
    }),
    };
    tileLayers['Základní'].addTo(map);
    L.control.layers(tileLayers).addTo(map);

    // Mapy.cz require us to place their logo somewhere
    const LogoControl = L.Control.extend({
      options: {
        position: 'bottomleft',
      },

      onAdd: function (map) {
        const container = L.DomUtil.create('div');
        const link = L.DomUtil.create('a', '', container);

        link.setAttribute('href', 'http://mapy.cz/');
        link.setAttribute('target', '_blank');
        link.innerHTML = '<img src="https://api.mapy.cz/img/api/logo.svg" />';
        L.DomEvent.disableClickPropagation(link);

        return container;
      },
    });
    new LogoControl().addTo(map);

    g_aOdtMap = map;
}

//---------------------------------------------------------------------
function odtZoomToUserLocation()
{
    if (g_aOdtMap)
    {
        navigator.geolocation.getCurrentPosition(position => {
            const { latitude, longitude } = position.coords;
            // check if inside Prague 12 district, else ignore as user would see empty map
            if (longitude > 14.39 && longitude < 14.468 &&
                latitude > 49.963 && latitude < 50.023)
            {
                g_aOdtMap.setView(new L.LatLng(latitude, longitude), 17);
            }
        });
    }
}

//---------------------------------------------------------------------
// Adds processed markers to the map
function odtRemoveAllMapMarkers()
{
    if (!g_aOdtMap) return false;
    
    g_aOdtMap.eachLayer((layer) => {
        if (layer instanceof L.Marker) {
           layer.remove();
        }
    });
}

//---------------------------------------------------------------------
// Adds processed markers to the map
function odtAddMapMarker(fGpsPosLong, fGpsPosLat, sTitle, sInfoText)
{
    if (!g_aOdtMap) return false;
    
    L.marker([fGpsPosLat, fGpsPosLong]).addTo(g_aOdtMap)
        .bindPopup('<big>' + sTitle + '</big><br/>' + sInfoText);
    return true;
}

//---------------------------------------------------------------------
// Main function to download and process all kontejnery CSV files. Pass array of urls and UI configuration as a parameter.
function odtDlData(aConfig, bUseTestData = false)
{
    var urls = bUseTestData ? aConfig.arrTestUrls : aConfig.arrOpenUrls;
	console.log("Downloading data for urls ", urls);
	
	// download all datasources (https://stackoverflow.com/questions/29676408/html-fetch-multiple-files)
	var list = [];
    var arrFetchResults = [];

    urls.forEach((url, i) => {
      list.push(
        fetch(url)
        .then((response) => {
            odtSetStatusBtn(aConfig.arrDlBtnIds, i, response.ok);
			if (!response.ok) {
			    throw new Error("HTTP error: " + response.status);
			}
            console.log("Downloaded ", url);
            return response.text();
        })
        .then((csvData) => {
            arrFetchResults[i] = csvData;
        })
		.catch((error) => {
            console.log("Cannot fetch data, error " + error);
            odtSetStatusBtn(aConfig.arrDlBtnIds, i, false);
		})
      );
    });
    
    Promise
    .all(list)
    .then(() => {
        //alert('all requests finished!');
        console.log("All download requests finished");
        aConfig.fnProcessDlResults(arrFetchResults, aConfig);
    });
}

//---------------------------------------------------------------------
// returns true when parsed csv data is valid. sReqColumns should be used to set if the columns name with diacritics is really present (if not, the file is probably not in UTF8)
function odtParsedDataOK(data, sReqColumn = undefined)
{
    return data             // parsed anything
        && data.length > 0  // ... with at least 1 row
        && Object.keys(data[0]).length > 1  //... and at least two columns
        && (!sReqColumn || (sReqColumn in data[0]));  // ... this column must be present (test diacritics)
}

//---------------------------------------------------------------------
// Status buttons
function odtSetStatusBtn(arrBtns, idx, bOK)
{
    var elBtn = document.getElementById(arrBtns[idx]);
    if (elBtn)
    {
        elBtn.classList.remove('is-loading');
        elBtn.classList.remove('is-white');
        elBtn.classList.remove('is-static');
        if (bOK) {
            elBtn.value = "OK";
            elBtn.classList.add('is-success');
        }
        else {
            elBtn.innerHTML = "chyba";
            elBtn.classList.add('is-danger');
        }
        elBtn.disabled = true;
    }
}

//---------------------------------------------------------------------
// Set countdown on start panel to automatically close it
function odtCloseModalWithCountdown()
{
    const countdown = async () => {
      const startTime = Date.now();
      const endTime = startTime + 4000; // 4 sec
      const btnClose = document.getElementById('odtBtnClose');
      btnClose.innerHTML = "Hotovo (4)";
      while (Date.now() < endTime) {
        await new Promise(resolve => setTimeout(resolve, 1000));
        const timeRemaining = endTime - Date.now();
        const seconds = Math.round(timeRemaining / 1000);
        if (seconds > 0)
            btnClose.innerHTML = `Hotovo (${seconds})`;
        else
            btnClose.innerHTML = "Hotovo";
      }
      //alert("Time's up!");
      btnClose.click();
    }
    countdown();
}

//---------------------------------------------------------------------
// Converts time from hh:mm:ss to hh:mm
function odtFormatTime(cas)
{
	return cas.slice(0, -3);
}

//---------------------------------------------------------------------
// Converts date from yyyy-mm-dd to dd.mm.yyyy
function odtFormatDateTime(datum)
{
    const iTime = Date.parse(datum);
    if (isNaN(iTime))
        return datum;
	var aDate = new Date();
	aDate.setTime(iTime);
    return aDate.getDate() + "." + (aDate.getMonth()+1) + "." + aDate.getFullYear();
}

//---------------------------------------------------------------------
// Converts number from "xx.xx" to localized string "xx,xx"
const g_aNumFormat = new Intl.NumberFormat("cs-CZ", { useGrouping: true, maximumFractionDigits: 2 });

function odtFormatFloat(val)
{
    var fVal = parseFloat(val);
    if (isNaN(fVal))
        return val;
    else
        return g_aNumFormat.format(fVal);
}

//---------------------------------------------------------------------
// HARMONOGRAMY PŘISTAVOVÁNÍ VELKOOBJEMOVÝCH KONTEJNERŮ
//---------------------------------------------------------------------

const VOK_IDX_LOKACE = 0;
const VOK_IDX_VOK = 1;
const VOK_IDX_BIO = 2;

//---------------------------------------------------------------------
// Uses PapaParse to process downloaded CSV files and calls function to combine them together
function vokProcessAllDownloadedData(arrFetchResults, aConfig)
{
    var arrParsedCsv = [];
    
    // parse synchronously (without Premise), as we have quite small files 
    arrFetchResults.forEach((csvData,i) => {
        var configAfterFetch = {
            header: true,
            skipEmptyLines: true,
            complete: (results ) => {
                var bOK = odtParsedDataOK(results.data);
                odtSetStatusBtn(aConfig.arrCsvBtnIds, i, bOK);
            	console.log("Papa parsed CSV file with index " + i + " success: ", bOK);
            	if (!bOK)
            	    console.log("Papa Results:", results);
                arrParsedCsv[i] = results.data;
            }
        }
        Papa.parse(csvData, configAfterFetch);
    });
    console.log("All processing requests finished, parsed CSV files: ", arrParsedCsv.length);
    
    // fail when we don't have locations and at least one data fine
    var bResultsUsable = odtParsedDataOK(arrParsedCsv[VOK_IDX_LOKACE]) &&
         (odtParsedDataOK(arrParsedCsv[VOK_IDX_VOK]) || odtParsedDataOK(arrParsedCsv[VOK_IDX_BIO]));
    
    if (bResultsUsable)
    {
        vokCombineHarmonogram(arrParsedCsv[VOK_IDX_LOKACE], arrParsedCsv[VOK_IDX_VOK], "VOK");
        vokCombineHarmonogram(arrParsedCsv[VOK_IDX_LOKACE], arrParsedCsv[VOK_IDX_BIO], "Bioodpad");
        vokFillOzpHarmonogramyMap(arrParsedCsv[VOK_IDX_LOKACE]);
        
        odtCloseModalWithCountdown();
    }
}

let vokDnyVTydnu = ['ne', 'po', 'út', 'st', 'čt', 'pá', 'so'];
const vokThisYear = new Date().getFullYear();

//---------------------------------------------------------------------
// converts date from yyyy-mm-dd to dd.mm a time from hh:mm:ss to hh:mm
function vokFormatDateTime(datum, cas)
{
	const aDate = new Date(datum);
	const iDen = aDate.getDay();
    var sDatum = vokDnyVTydnu[iDen] + " " + aDate.getDate() + "." + (aDate.getMonth()+1);
	if (aDate.getFullYear() != vokThisYear)
	    sDatum += "." + aDate.getFullYear();
	return sDatum + " " + odtFormatTime(cas);
}

//---------------------------------------------------------------------
// Adds harmonogram data to the locations array
function vokCombineHarmonogram(arrLokace, arrHarmonogram, sTypeName)
{
    if (!odtParsedDataOK(arrHarmonogram) || !odtParsedDataOK(arrHarmonogram))
        return;
        
    var bMaUcel = ("materiál" in arrHarmonogram[0]); // lines contain material
    arrHarmonogram.forEach(row => {
        // find it inside lokace
        var rowLokace = arrLokace.find(el => el.název === row.stanoviště);
        if (rowLokace)
        {
            // datum
            var sDatum = vokFormatDateTime(row.datum_od, row.čas_od) + " - ";
            if (row.datum_od === row.datum_do)
                sDatum += odtFormatTime(row.čas_do);
            else
                sDatum += vokFormatDateTime(row.datum_do, row.čas_do);
    
            var sInfo = sDatum + " " + sTypeName;
            if (bMaUcel)
                sInfo += " (" + row.materiál + ")";

            // append info
            if (!("harmonogram" in rowLokace))
                rowLokace.harmonogram = [{datum_od: row.datum_od, text: sInfo}];
            else
                rowLokace.harmonogram.push({datum_od: row.datum_od, text: sInfo});
        }
        else
        {
            console.log("Varování: nelze najít stanoviště " + row.stanoviště + " použité v souboru " + sTypeName);
        }
    });
}

//---------------------------------------------------------------------
// Adds processed markers to the map
function vokFillOzpHarmonogramyMap(arrLokace)
{
    odtRemoveAllMapMarkers();
    arrLokace.forEach(rowLokace => {

        if (!("harmonogram" in rowLokace))  // skip locations with no kontejners
            return;
            
        // sort harmonogram by datum
        rowLokace.harmonogram.sort((item1,item2) => (item1.datum_od > item2.datum_od) ? 1 : -1);
        const sInfoHtml = rowLokace.harmonogram.map(item => item.text).join("<br>");

        odtAddMapMarker(rowLokace.pozice_long, rowLokace.pozice_lat, rowLokace.název, sInfoHtml);
    });
}

//---------------------------------------------------------------------
// PRONÁJMY NEBYTOVÝCH PROSTOR
//---------------------------------------------------------------------
// Uses PapaParse to process downloaded CSV files and calls function to combine them together
function majProcessAllDownloadedData(arrFetchResults, aConfig)
{
    var arrParsedCsv = [];
    
    // parse synchronously (without Premise), as we have quite small files 
    arrFetchResults.forEach((csvData,i) => {
        var configAfterFetch = {
            header: true,
            skipEmptyLines: true,
            complete: (results ) => {
                var bOK = odtParsedDataOK(results.data, "roční_nájem_výše");
                if (aConfig)
                    odtSetStatusBtn(aConfig.arrCsvBtnIds, i, bOK);
            	console.log("Papa parsed CSV file with index " + i + " success: ", bOK);
            	if (!bOK)
            	    console.log("Papa Results:", results);
                arrParsedCsv[i] = results.data;
            }
        }
        Papa.parse(csvData, configAfterFetch);
    });
    console.log("All processing requests finished, parsed CSV files: ", arrParsedCsv.length);
    
    // fail when we don't have locations and at least one data fine
    var bResultsUsable = odtParsedDataOK(arrParsedCsv[0], "roční_nájem_výše");
    
    if (bResultsUsable)
    {
        majCombinePlaces(arrParsedCsv[0]);
        majFillMap(arrParsedCsv[0]);
        
        if (aConfig)
            odtCloseModalWithCountdown();
    }
}

//---------------------------------------------------------------------
function majFormatInfoText(item)
{
    var sInfoText = "<p>";
    if ("typ_prostoru" in item)
    {
        sInfoText += `<b>Typ prostoru:</b> ${item.typ_prostoru}`;
        if ("typ_prostoru" in item)
            sInfoText += " - " + odtFormatFloat(item.plocha_m2) + " m<sup>2</sup>";
    }
    if ("nájemce" in item)
        sInfoText += `<br/><b>Nájemce:</b> ${item.nájemce}`;
    if ("smlouva_od" in item)
        sInfoText += " (od " + odtFormatDateTime(item.smlouva_od) + ")";
    if ("roční_nájem_výše" in item)
    {
        sInfoText += "<br/>"
        sInfoText += "<b>Roční nájemné:</b> " + odtFormatFloat(item.roční_nájem_výše) + " Kč";
        if ("nájem_1m2" in item)
            sInfoText += " (" + odtFormatFloat(item.nájem_1m2) + " Kč/m<sup>2</sup>)";
    }
    return sInfoText;
}

//---------------------------------------------------------------------
// Have all rooms in one building (same GPS) under the same marker
function majCombinePlaces(arrLokace)
{
    var setIdxDone = new Set();
    
    arrLokace.forEach((item, idx) => {
        if (setIdxDone.has(idx))
            return;
        
        var sInfoText = majFormatInfoText(item);
        // find other items with the same location
        setIdxDone.add(idx);
                
        arrLokace.forEach((item2, idx2) => {
            if (idx2 <= idx || item2.pozice_long !== item.pozice_long || item2.pozice_lat !== item.pozice_lat)
                return;
            
            setIdxDone.add(idx2);
            const sInfoText2 = majFormatInfoText(item2);
            if (sInfoText2 && sInfoText2.length > 0)
                sInfoText += sInfoText2;
        });
        
        // store resulting combined info text
        item.infoText = sInfoText;
    });
}

//---------------------------------------------------------------------
// Adds processed markers to the map
function majFillMap(arrLokace)
{
    odtRemoveAllMapMarkers();
    arrLokace.forEach(item => {
        if ("infoText" in item)  // only items that has info text 
            odtAddMapMarker(item.pozice_long, item.pozice_lat, item.objekt, item.infoText);
    });
}
