function handleCsvFiles(files) {
	console.log("CSV files to handle:", files[0]);

	if (!window.FileReader){
		alert('FileReader is not supported in this browser.');
		return;
	}

    var reader = new FileReader();
    reader.onload = function(event) {
        var csv = event.target.result;
        var config = {
            header: true,
            skipEmptyLines: true,
            complete: completeCsvParseFn
        }
        Papa.parse(csv, config);
    }
    reader.onerror = function(event) {
        console.log(event);
        if(event.target.error.name == "NotReadableError") {
            alert("Cannot read file!");
        }
    };
    
    // Read file into memory as UTF-8
    reader.readAsText(files[0]);	
}

//---------------------------------------------------------------------
function handleCsvFileInUrl(url) {
	console.log("URL with CSV file to handle:", url);

	/*
	// this should work fine on same-server URL (test data)
	var config = {
		download: true,
		header: true,
		skipEmptyLines: true,
		complete: completeCsvParseFn
	}
	Papa.parse(url, config);
	*/
	
	fetch(url, {referrerPolicy: 'unsafe-url'})
		.then((response) => {
			if (!response.ok) {
			  throw new Error(`HTTP error: ${response.status}`);
			}
			return response.text();
		})
		.then((text) => {
			var configAfterFetch = {
				header: true,
				skipEmptyLines: true,
				complete: completeCsvParseFn
			}
			Papa.parse(text, configAfterFetch)
		})
		.catch((error) => {
			document.getElementById("csvOutput").innerHTML = `<p style="color:red">Could not fetch data: ${error}; (also check the Console output in your browser)</p>`;
			document.getElementById("csvOutput_raw").textContent = "";
		});

	//fetch(url, {mode: 'no-cors', referrerPolicy: 'unsafe-url'})
	//fetch(url, {mode: 'cors', referrer: 'https://storage.golemio.cz', referrerPolicy: 'unsafe-url'})
}

function completeCsvParseFn(results)
{
	//console.log("Papa Results:", results);
	drawOutputAsObj(results.data);
}

//---------------------------------------------------------------------
function drawDefaultTableOutput(lines, table) {
	//for the table headings
	var tableHeader = table.insertRow(-1);
 	Object.keys(lines[0]).forEach(function(key){
 		var el = document.createElement("TH");
		el.innerHTML = key;
		tableHeader.appendChild(el);
	});

	//the data
	for (var i = 0; i < lines.length; i++) {
		var row = table.insertRow(-1);
		Object.keys(lines[0]).forEach(function(key){
			var data = row.insertCell(-1);
			data.appendChild(document.createTextNode(lines[i][key]));
		});
	}
}

//---------------------------------------------------------------------
// prevede cas z hh:mm:ss do hh:mm
function formatovatCas(cas) {
	return cas.slice(0, -3);
}

//---------------------------------------------------------------------
let dnyVTydnu = ['ne', 'po', 'út', 'st', 'čt', 'pá', 'so'];

function formatovatDatum(datum) {
	const aDate = new Date(datum);
	const iDen = aDate.getDay();
	return dnyVTydnu[iDen] + ' ' + aDate.getDate() + '.' + (aDate.getMonth()+1) + '.';
}

// prevede datum z yyyy-mm-dd do dd.mm a cas z hh:mm:ss do hh:mm
function formatovatDatumACas(datum, cas) {
	return formatovatDatum(datum) + ' ' + formatovatCas(cas);
}

//---------------------------------------------------------------------
//draw the table, if first line contains heading
function drawOutputAsObj(lines){
	//Clear previous data
	document.getElementById("csvOutput").innerHTML = "";
	var table = document.createElement("table");
	table.classList.add('table');  // for bulma.css
	table.classList.add('is-narrow');

    var sMonthsList = '';
	if (lines[0].length < 5) {
		drawDefaultTableOutput(lines, table);
	}
	else {
        var iLastMonth = 0;

		var bHasMaterial = ("materiál" in lines[0]); // radky obsahuji material
		
		for (var i = 0; i < lines.length; i++) {
		    
			var row = table.insertRow(-1);
			// datum a cas
			if (lines[i].datum_od === lines[i].datum_do) {
				var sDatum = formatovatDatum(lines[i].datum_od);
				var sCas = formatovatCas(lines[i].čas_od) + ' - ' + formatovatCas(lines[i].čas_do);
				
                var cellDate = row.insertCell(-1);
                cellDate.style.textAlign = "center";
                cellDate.innerHTML = sDatum;

                var cellTime = row.insertCell(-1);
                cellTime.style.textAlign = "center";
                cellTime.innerHTML = sCas;
			}
			else {
				// je to rozmezi vice dni - one cell with colspan 2
				var sDatum = formatovatDatumACas(lines[i].datum_od, lines[i].čas_od) + ' - ' + formatovatDatumACas(lines[i].datum_do, lines[i].čas_do);
				
                var cellDate = row.insertCell(-1);
                cellDate.colSpan = 2;
                cellDate.style.textAlign = "center";
                cellDate.innerHTML = sDatum;
			}
			
			// nazev stanoviste
			var cellLok = row.insertCell(-1);
			var sStanoviste = lines[i].stanoviště;
			if (bHasMaterial)
				sStanoviste = sStanoviste + ' (' + lines[i].materiál + ')'; 
				
			// link here when new month starts
		    const aDate = new Date(lines[i].datum_od);
            var iMonth = (aDate.getMonth()+1);
            if (iMonth != iLastMonth)
            {
                sStanoviste = "<a name='mesic" + iMonth + "'></a>" + sStanoviste;
                iLastMonth = iMonth;
                
                // add link to here from the upper navigation
                const sMonthName = aDate.toLocaleString('default', { month: 'long' });
                sMonthsList += "<a href='#mesic" + iMonth + "'>" + sMonthName + "</a> &nbsp; &nbsp; "
            }
			cellLok.innerHTML = sStanoviste;
		}
	}
	// months switcher
    var elMonths = document.createElement("p");
    elMonths.innerHTML = sMonthsList;
    document.getElementById("csvOutput").appendChild(elMonths);
    // nice looking table for praha12.cz site
	document.getElementById("csvOutput").appendChild(table);
	// html code to copy to praha12.cz site
	document.getElementById("csvOutput_raw").textContent = '<p>' + sMonthsList  + '</p>\n<table>' + table.innerHTML + '</table>';
	table.scrollIntoView({ behavior: "smooth", block: "nearest" });
}

//---------------------------------------------------------------------
function logExcelMsgClear()
{
    var elOutput = document.getElementById("xlsOutput");
	elOutput.innerHTML = ``;
}

function logExcelMsgAddLine(sMsg)
{
    var elOutput = document.getElementById("xlsOutput");
	elOutput.innerHTML = elOutput.innerHTML + "<br>" + sMsg;
}

//---------------------------------------------------------------------
function exportGpsLocations(aSheet, sButtonId, aoaLokace)
{
    resetDownloadData(sButtonId);

    logExcelMsgAddLine("Probiha zpracovani listu lokace-gps");
    if (aSheet === undefined)
    {
        logExcelMsgAddLine("Chyba: nelze najít list lokace-gps");
        return;
    }

	var config = [
		{ excSloupec: "A", NazevSloupce: "název", DataTyp: "odtRetezec" },
		{ excSloupec: "B", NazevSloupce: "pozice_long", DataTyp: "odtCislo" },
		{ excSloupec: "C", NazevSloupce: "pozice_lat", DataTyp: "odtCislo" }
	];
	var sCsvData = transformExcelSheetToCsv(aSheet, config, logExcelMsgAddLine, aoaLokace);
	//console.log(sCsvData);
	storeDownloadData(sCsvData, sButtonId);
}

//---------------------------------------------------------------------
function exportOzpHarmonogramAddToOutput(sSheetName, arrOutputRows, bMaUcel, aoaLokace, sAktLok, sAktUcel, sAktDatumOd, sAktDatumDo, sAktCasOd, sAktCasDo)
{
    var arrRowVals = [];
    arrRowVals.push(sAktLok);
    arrRowVals.push(sAktDatumOd);
    arrRowVals.push(sAktCasOd);
    arrRowVals.push(sAktDatumDo);
    arrRowVals.push(sAktCasDo);
    if (bMaUcel)
        arrRowVals.push(sAktUcel);
    arrOutputRows.push(arrRowVals);
    if (aoaLokace)
    {
        // push to map
        var rowLokace = aoaLokace.find(elRow => elRow[0] === sAktLok);
        if (rowLokace)
        {
            var sInfo = sSheetName + " " + sAktDatumOd + " " + sAktCasOd + " - ";
            if (sAktDatumDo != sAktDatumDo)
                sInfo += sAktDatumOd + " ";
            sInfo += sAktCasDo;
            if (bMaUcel)
                sInfo += " " + sAktUcel;
            // append info
            if (rowLokace.length < 4)
                rowLokace.push(sInfo)
            else
                rowLokace[3] += "<br>" + sInfo;
        }
    }
}

//---------------------------------------------------------------------
function exportOzpHarmonogam(aSheet, sSheetName, sButtonId, sButtonShowCsvId, aoaLokace, bMaUcel)
{
    resetDownloadData(sButtonId);
    document.getElementById(sButtonShowCsvId).disabled = true;
    
    logExcelMsgAddLine("Probiha zpracovani listu " + sSheetName);
    if (aSheet === undefined)
    {
        logExcelMsgAddLine("Chyba: nelze najít list " + sSheetName);
        return;
    }
    
    // autodetect columns (find all occurrences of "čas")
    var arrDataColStarts = [];
    aSheet["!data"][0].forEach((cell, idxCol) => {
        if (cell.w === "čas")
            arrDataColStarts.push(idxCol);
    });
    
    if (arrDataColStarts.length === 0)
    {
        logExcelMsgAddLine("Chyba: nenalezen žádný sloupec `čas`");
        return;
    }
    
    logExcelMsgAddLine("Časové sloupce detekovány na: " + arrDataColStarts.map(idxCol => XLSX.utils.encode_col(idxCol)));

    // autodetect missing ucel, when not enough source columns
    if (bMaUcel && arrDataColStarts.length > 1 && arrDataColStarts[1]-arrDataColStarts[0] < 3)
    {
        bMaUcel = false;
    }
    
    // make array of arrays, then convert it to sheet and CSV file
    var arrOutputRows = [];
    arrDataColStarts.forEach(idxStart => {
        var sAktLok = "";
        var sAktUcel = "";
        var sAktDatumOd = "";
        var sAktDatumDo = "";
        var sAktCasOd = "";
        var sAktCasDo = "";

        aSheet["!data"].forEach((row, idxRow, array) => {
            if (idxRow === 0) return;  // skip header row
        
            var sNovyLok = row[idxStart+1] ? row[idxStart+1].w : "";
            var sNovyUcel = (bMaUcel && row[idxStart+2]) ? row[idxStart+2].w : "";
            var sNovyDatum = formatDate(row[0]);
            var sNovyCasOd = "";
            var sNovyCasDo = "";
            if (row[idxStart])
            {
                var arrTimeFromTo = row[idxStart].w.split(' - '); // cas
                if (arrTimeFromTo.length == 2)
                {
                    sNovyCasOd = arrTimeFromTo[0];
                    sNovyCasDo = arrTimeFromTo[1];
                    if (sNovyCasOd.length === 5) sNovyCasOd += ":00";   // add seconds
                    if (sNovyCasDo.length === 5) sNovyCasDo += ":00";
                    if (sNovyCasOd.length === 4) sNovyCasOd = "0" + sNovyCasOd + ":00";   // prefix hours and add seconds
                    if (sNovyCasDo.length === 4) sNovyCasDo = "0" + sNovyCasDo + ":00";
                }
            }
            if (sNovyLok && !sNovyCasOd)
            {
                logExcelMsgAddLine("Chyba: nelze rozdělit čas na rozmezí v buňce " + XLSX.utils.encode_col(idxStart) + (idxRow+1));
            }
            if (sNovyLok && aoaLokace)
            {
                // verify data integrity: find stanoviste in sheet lokace-gps
                if (!aoaLokace.find(rowLokace => rowLokace[0] === sNovyLok))
                    logExcelMsgAddLine("Varování: nelze najít stanoviště v buňce " + XLSX.utils.encode_col(idxStart+1) + (idxRow+1) + " ('" + sNovyLok + "') v listu lokace-gps");
            }
                     
            if (sNovyLok !== sAktLok)
            {
                // jina hodnota - dat do vystupu hotovou lokaci z prubeznych promennych
                if (sAktLok)
                {
                    exportOzpHarmonogramAddToOutput(sSheetName, arrOutputRows, bMaUcel, aoaLokace,
                        sAktLok, sAktUcel, sAktDatumOd, sAktDatumDo, sAktCasOd, sAktCasDo);
                }
            
                // nabrat si nove hodnoty do prubeznych promennych
                sAktLok = sNovyLok
                if (sNovyLok)
                {
                    sAktDatumOd = sNovyDatum;
                    sAktCasOd = sNovyCasOd;
                    sAktDatumDo = sNovyDatum;
                    sAktCasDo = sNovyCasDo;
                    sAktUcel = sNovyUcel;
                }
            }
            else if (sNovyLok === sAktLok && sNovyLok)
            {
                // stejna hodnota - dat dohromady datumy z predchoziho radu
                sAktDatumDo = sNovyDatum;
                sAktCasDo = sNovyCasDo;
            }
            if (sAktLok && idxRow === array.length-1) // posledni radek, dat do vystupu taky
            {
                exportOzpHarmonogramAddToOutput(sSheetName, arrOutputRows, bMaUcel, aoaLokace,
                    sAktLok, sAktUcel, sAktDatumOd, sAktDatumDo, sAktCasOd, sAktCasDo);
            }
        });
    });
    
    // sort by datum
    arrOutputRows.sort((row1,row2) => (row1[1] > row2[1]) ? 1 : -1);

    // add header as the first line
    var arrHeader = ["stanoviště","datum_od","čas_od","datum_do","čas_do"];
    if (bMaUcel)
        arrHeader.push("materiál");
    arrOutputRows.unshift(arrHeader);

    //console.log(arrOutputRows);
    logExcelMsgAddLine("Zpracováno záznamů: " + arrOutputRows.length);
    
    var ws = XLSX.utils.aoa_to_sheet(arrOutputRows, {});
    var sCsvData = XLSX.utils.sheet_to_csv(ws);
	storeDownloadData(sCsvData, sButtonId);
    document.getElementById(sButtonShowCsvId).disabled = false;
}

//---------------------------------------------------------------------
function handleExcelTable(files)
{
	console.log("Excel files to handle:", files[0]);
	logExcelMsgClear();

	if (!window.FileReader){
		alert('FileReader is not supported in this browser.');
		return;
	}

    var reader = new FileReader();
    reader.onload = function(event) {
        var data = event.target.result;
        var workbook = XLSX.read(data, { type: 'binary', dense: true });
        logExcelMsgAddLine("Excelový soubor načten OK s listy: " + workbook.SheetNames.join())
        
        var aoaLokace = [];
        exportGpsLocations(workbook.Sheets["lokace-gps"], "dlCsvGpsLoc", aoaLokace);
        exportOzpHarmonogam(workbook.Sheets["Bioodpad"], "Bioodpad", "dlCsvBIO", "dlCsvBIOShowCsv", aoaLokace, true);
        exportOzpHarmonogam(workbook.Sheets["VOK"], "VOK", "dlCsvVOK", "dlCsvVOKShowCsv", aoaLokace, false);
        //console.log(aoaLokace);
        fillOzpHarmonogramyMap(aoaLokace);
    }
    reader.onerror = function(event) {
        console.log(event);
        if(event.target.error.name == "NotReadableError") {
            alert("Nelze načíst soubor");
        }
    };
    
    reader.readAsBinaryString(files[0]);
}

//---------------------------------------------------------------------
function showGeneratedCsv(sButtonId)
{
    var sCsvData = getStoredDownloadData(sButtonId);
    if (sCsvData)
    {
        var config = {
            header: true,
            skipEmptyLines: true,
            complete: completeCsvParseFn
        }
        Papa.parse(sCsvData, config);
    }
}

//---------------------------------------------------------------------
var g_aMapOzp;
function initOzpHarmonogramyMap()
{
    // Init maps with Leaflet
    const MAPY_API_KEY = "ptZVPljDvizQ0cegplSTPFbdOj2NZzIkOxg_QfZT_Jo";
    var map = L.map('m').setView([49.996, 14.44], 13);
    const tileLayers = {
      'Základní': L.tileLayer(`https://api.mapy.cz/v1/maptiles/basic/256/{z}/{x}/{y}?apikey=${MAPY_API_KEY}`, {
        minZoom: 0,
        maxZoom: 19,
        attribution: '<a href="https://api.mapy.cz/copyright" target="_blank">&copy; Seznam.cz a.s. a další</a>',
    }),
      'Letecká': L.tileLayer(`https://api.mapy.cz/v1/maptiles/aerial/256/{z}/{x}/{y}?apikey=${MAPY_API_KEY}`, {
        minZoom: 0,
        maxZoom: 19,
        attribution: '<a href="https://api.mapy.cz/copyright" target="_blank">&copy; Seznam.cz a.s. a další</a>',
    }),
    };
    tileLayers['Základní'].addTo(map);
    L.control.layers(tileLayers).addTo(map);

    // Mapy.cz require us to place their logo somewhere
    const LogoControl = L.Control.extend({
      options: {
        position: 'bottomleft',
      },

      onAdd: function (map) {
        const container = L.DomUtil.create('div');
        const link = L.DomUtil.create('a', '', container);

        link.setAttribute('href', 'http://mapy.cz/');
        link.setAttribute('target', '_blank');
        link.innerHTML = '<img src="https://api.mapy.cz/img/api/logo.svg" />';
        L.DomEvent.disableClickPropagation(link);

        return container;
      },
    });
    new LogoControl().addTo(map);

    g_aMapOzp = map;
}

//---------------------------------------------------------------------
function fillOzpHarmonogramyMap(aoaLokace)
{
    if (!g_aMapOzp) return;
    
    aoaLokace.forEach((row, idx) => {
        var markerText = "<big>" + row[0] + "</big><br/>"; // stanoviste
        if (row.length > 3 && row[3])
            markerText += "<div class='content is-small'>" + row[3] + "</div>";
        else
            markerText += "žádné kontejnery nebudou přistaveny";

        if (!isNaN(row[2]) && !isNaN(row[1]))
            L.marker([row[2], row[1]]).addTo(g_aMapOzp)
                .bindPopup(markerText);
    });
}

//---------------------------------------------------------------------
function majExportLocations(aSheet, sButtonId, aoaLokace)
{
    resetDownloadData(sButtonId);

    logExcelMsgAddLine("Probiha zpracovani");
    if (aSheet === undefined)
    {
        logExcelMsgAddLine("Chyba: nelze najít list s daty");
        return;
    }

	var config = [
		{ excSloupec: "A", NazevSloupce: "objekt", DataTyp: "odtRetezec" },
		{ excSloupec: "B", NazevSloupce: "nájemce", DataTyp: "odtRetezec" },
		{ excSloupec: "C", NazevSloupce: "typ_prostoru", DataTyp: "odtRetezec" },
		{ excSloupec: "F", NazevSloupce: "plocha_m2", DataTyp: "odtCislo" },
		{ excSloupec: "G", NazevSloupce: "roční_nájem_výše", DataTyp: "odtCislo", PridatCZK: "roční_nájem_měna" },
		{ excSloupec: "I", NazevSloupce: "nájem_1m2", DataTyp: "odtCislo" },
		{ excSloupec: "J", NazevSloupce: "smlouva_od", DataTyp: "odtDatum" },
		{ excSloupec: "K", NazevSloupce: "smlouva_do", DataTyp: "odtDatum" },
		{ excSloupec: "O", NazevSloupce: "pozice_lat", DataTyp: "odtCislo", Required: true }, // delete rows not containing this value
		{ excSloupec: "P", NazevSloupce: "pozice_long", DataTyp: "odtCislo" }
	];
	var sCsvData = transformExcelSheetToCsv(aSheet, config, logExcelMsgAddLine, aoaLokace);
	//console.log(sCsvData);
	storeDownloadData(sCsvData, sButtonId);
}

//---------------------------------------------------------------------
function majHandleExcelTable(files)
{
	console.log("Excel files to handle:", files[0]);
	logExcelMsgClear();

	if (!window.FileReader){
		alert('FileReader is not supported in this browser.');
		return;
	}

    var reader = new FileReader();
    reader.onload = function(event) {
        var data = event.target.result;
        var workbook = XLSX.read(data, { type: 'binary', dense: true });
        logExcelMsgAddLine("Excelový soubor načten OK s listy: " + workbook.SheetNames.join())
        
        var aoaLokace = [];
        majExportLocations(workbook.Sheets[workbook.SheetNames[0]], "dlMajCsv", aoaLokace);
        //console.log(aoaLokace);
        // show on map
        var csv = Papa.unparse(aoaLokace);
        majProcessAllDownloadedData([csv], null);
    }
    reader.onerror = function(event) {
        console.log(event);
        if(event.target.error.name == "NotReadableError") {
            alert("Nelze načíst soubor");
        }
    };
    
    reader.readAsBinaryString(files[0]);
}