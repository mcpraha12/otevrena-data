//----------------------------------------
// for XLSX documentation see https://docs.sheetjs.com/
//----------------------------------------
function formatDate(aCell)
{
    //console.log(aCell);
    if (!aCell || !aCell.t)
        return "";
    else if (aCell.t === "d") // date
        return aCell.v.toISOString().substring(0,10);
    else if (aCell.t === "n") // usually numberFormat 14 (m/d/yy)
    {
        var arrComp = aCell.w.split("/");
        if (arrComp.length === 3)
        {
            if (arrComp[0].length < 2)  // month
                arrComp[0] = "0" + arrComp[0];
            if (arrComp[1].length < 2) 
                arrComp[1] = "0" + arrComp[1];
            if (arrComp[2].length == 2) 
                arrComp[2] = "20" + arrComp[2];
            return arrComp[2] + "-" + arrComp[0] + "-" + arrComp[1];
        }
        else
            return aCell.w;
    }
    else if (aCell.t === "s") // string, probably in czech format (dd.mm.yyyy)
    {
        var arrComp = aCell.w.split(".");
        if (arrComp.length === 3)
        {
            if (arrComp[0].length < 2)  // month
                arrComp[0] = "0" + arrComp[0];
            if (arrComp[1].length < 2) 
                arrComp[1] = "0" + arrComp[1];
            if (arrComp[2].length == 2) 
                arrComp[2] = "20" + arrComp[2];
            //console.log(aCell.w + " -> " + arrComp[2] + "-" + arrComp[1] + "-" + arrComp[0]);
            return arrComp[2] + "-" + arrComp[1] + "-" + arrComp[0];
        }
        else
            return aCell.w;
    }
    else
        return aCell.w;
}

//---------------------------------------------------------------------
function formatNumber(aCell)
{
    //console.log(aCell);
    if (!aCell || !aCell.t)
        return "";
    else if (aCell.t === "n")
        return aCell.v;
    else
        return aCell.w;
}

//---------------------------------------------------------------------
function transformExcelSheetToCsv(aSheet, config, fnLogOutput, aoaOutput = null)
{
    var arrOutputRows = [];     // make array of array, then convert it to sheet and CSV file
    // output header
    var arrHeader = [];
    config.forEach(col => {
        arrHeader.push(col.NazevSloupce);
        if (col.PridatCZK)
            arrHeader.push(col.PridatCZK);
    });

    arrOutputRows.push(arrHeader)
    
    var sError = "";
    // find column indices in the sheet
    config.forEach(col => {
        if (col.excSloupec !== undefined)
        {
            col["excIndex"] = XLSX.utils.decode_col(col.excSloupec);
        }
        else if (col.NazevPuvodni !== undefined)
        {
            // find it in sheet header
            aSheet["!data"][0].forEach((cell, loopindex) => {
                if (cell === col.NazevPuvodni)
                {
                    col["excIndex"] = loopindex;
                }
            });
            if (col.excIndex === undefined)
            {
                sError = "Kritická chyba: sloupec s nazvem \"" + col.NazevPuvodni + "\" nebyl nalezen. Pravděpodobně se změnily názvy sloupců.";
                return;
            }
        }
        if (col.excIndex === undefined)
        {
            sError = "Kritická chyba: nedefinována vstupní data pro sloupec \"" + col.NazevSloupce;
            return;
        }
        console.log(col);
    });
    if (sError != "")
    {
        fnLogOutput(sError);
        return "";
    }
    
    aSheet["!data"].forEach((row, idxRow) => {
        if (idxRow == 0) return;  // skip header row

        var bRowOK = true;
        var arrRowVals = [];
        config.forEach(col => {
            var aTxValue;
            if (row[col.excIndex] === undefined)
            {
                if (col.Required)
                    bRowOK = false;
                aTxValue = "";
            }
            else
            {
                // format cell when needed
                var aCell = row[col.excIndex];
                if (col.DataTyp === "odtDatum")
                    aTxValue = formatDate(aCell);
                else if (col.DataTyp === "odtCislo")
                    aTxValue = formatNumber(aCell);
                else
                    aTxValue = aCell.w;
            }
            arrRowVals.push(aTxValue);
            
            if (col.PridatCZK)
                arrRowVals.push("CZK");
        });
        if (bRowOK)
            arrOutputRows.push(arrRowVals);
    });
    
    fnLogOutput("Zpracováno záznamů: " + arrOutputRows.length);
    
    if (aoaOutput)
    {
        arrOutputRows.map(itRow => aoaOutput.push(itRow)); // copies reference to nested arrays
    }
    
    var ws = XLSX.utils.aoa_to_sheet(arrOutputRows, {});
    return XLSX.utils.sheet_to_csv(ws);
}

var g_parsedVokCSVData = {};
//---------------------------------------------------------------------
function resetDownloadData(sButtonId)
{
	var elLink = document.getElementById(sButtonId);
	elLink.href = "";
	elLink.classList.remove('is-link');
	elLink.classList.add('is-static');
    g_parsedVokCSVData[sButtonId] = null;
}

//---------------------------------------------------------------------
function storeDownloadData(sCsvData, sButtonId)
{
	var blob = new Blob([sCsvData], {type: 'text/csv'});
	var elLink = document.getElementById(sButtonId);
	elLink.href = window.URL.createObjectURL(blob);
	elLink.classList.remove('is-static');
	elLink.classList.add('is-link');
    g_parsedVokCSVData[sButtonId] = sCsvData;
}

//---------------------------------------------------------------------
function getStoredDownloadData(sButtonId)
{
    return g_parsedVokCSVData[sButtonId];
}
