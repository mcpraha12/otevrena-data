var TABS = [];
var CONTENT = [];

document.addEventListener('DOMContentLoaded', () => {

 // https://stackoverflow.com/questions/47591474/how-to-switch-through-tabs-in-bulma
  TABS = document.querySelectorAll('#tabs li');
  CONTENT = document.querySelectorAll('#tab-content form');
  initTabs();
});

function initTabs() {
    TABS.forEach((tab) => {
      tab.addEventListener('click', (e) => {
        let selected = tab.getAttribute('data-tab');
        updateActiveTab(tab);
        updateActiveContent(selected);
      })
    })
}

function updateActiveTab(selected) {
  TABS.forEach((tab) => {
    if (tab && tab.classList.contains('is-active')) {
      tab.classList.remove('is-active');
    }
  });
  selected.classList.add('is-active');
}

function updateActiveContent(selected) {
  CONTENT.forEach((item) => {
    if (item && item.classList.contains('is-active')) {
      item.classList.remove('is-active');
      item.style.display = "none";
    }
    let data = item.getAttribute('data-content');
    if (data === selected) {
      item.classList.add('is-active');
      item.style.display = "block";
    }
  });
}
